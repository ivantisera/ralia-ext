import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'


Vue.config.productionTip = false;
//Vue.prototype.BASE_URL = "http://127.0.0.1:3000/";

Vue.prototype.BASE_URL = "https://calm-falls-92453.herokuapp.com/";

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
